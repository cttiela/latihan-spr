﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VoterAnalysis.Startup))]
namespace VoterAnalysis
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
