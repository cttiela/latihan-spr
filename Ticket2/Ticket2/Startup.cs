﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ticket2.Startup))]
namespace Ticket2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
