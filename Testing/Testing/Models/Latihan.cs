﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Testing.Models
{
    public class Latihan
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Anjuran { get; set; }
        public DateTime? TarikhMula { get; set; }
        public DateTime? TarikhAkhir { get; set; }
        public int BilHari { get; set; }
    }
}