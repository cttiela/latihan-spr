﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Latihan.Startup))]
namespace Latihan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
